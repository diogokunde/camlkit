//
//  ViewController.swift
//  camlKit-Example
//
//  Created by Simon Mcloughlin on 18/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import UIKit
import camlKit

class ViewController: UIViewController {
	
	let tezosNodeClient = TezosNodeClient(config: TezosNodeClientConfig(withDefaultsForNetworkType: .testnet))
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Performance Test of creating and re-creating wallet
		/*
		print("Start 1: \(Date())")
		Wallet.create(withMnemonicOfLength: .twelve) { (wallet) in
			print("End 1: \(Date())")
			print("wallet.address 1: \(wallet?.address ?? "")\n")
			
			DispatchQueue.main.asyncAfter(deadline: .now()+3) {
				print("Start 2: \(Date())")
				let wallet = Wallet.restoreFromKeychain()
				print("End 2: \(Date())")
				print("wallet.address 2: \(wallet?.address ?? "")")
			}
		}
		*/
		
		// tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb
		/*
		Wallet.create(withMnemonic: "else topic alcohol cargo session else razor remain horn object dignity help", writeToKeychain: true) { [weak self] (wallet) in
			guard let wallet = wallet else {
				print("Error creating wallet")
				return
			}
			
			self?.tezosNodeClient.send(operations: [], withWallet: wallet)
		}*/
		
		
		guard let wallet = Wallet.restoreFromKeychain() else {
			print("Error creating wallet")
			return
		}
		
		let operations = OperationFactory.sendOperation(XTZAmount(fromNormalisedAmount: 0.25), of: Token.xtz(), from: wallet.address, to: "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF")
		
		tezosNodeClient.estimate(operations: operations, withWallet: wallet) { [weak self] (result) in
			switch result {
				case .success(let ops):
				
					self?.tezosNodeClient.send(operations: ops, withWallet: wallet) { (result) in
						switch result {
							case .success(let string):
								print("\n\nAPPLICATION - SEND SUCCESS: \(string)")
							
							case .failure(let error):
								print("\n\nAPPLICATION - SEND FAILURE: \(error)")
						}
					}
				
				case .failure(let error):
					print("\n\nAPPLICATION - SEND FAILURE: \(error)")
			}
		}
		
		
		/*
		tezosNodeClient.send(operations: operations, withWallet: wallet) { (result) in
			switch result {
				case .success(let string):
					print("\n\nAPPLICATION - SEND SUCCESS: \(string)")
				
				case .failure(let error):
					print("\n\nAPPLICATION - SEND FAILURE: \(error)")
			}
		}
		*/
		
		
		
		// Test on fetching balance
		/*
		tezosNodeClient.getBalance(forAddress: "tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb") { (result) in
			switch result {
				case .success(let balance):
					print("\n\n\n Success: \(balance) \n\n\n")
				
				case .failure(let error):
					print("\n\n\n Failure: \(error) \n\n\n")
			}
		}
		*/
		
		
		
		
		
		// Test Token object
		
		/*
		let xtz = Token.xtz()
		let xtz2 = Token.xtz()
		let xtz3 = Token.xtz()
		
		let usdtz = Token(icon: nil, name: "USD Tez", symbol: "USDtz", tokenType: .fa1_2, decimalPlaces: 8, tokenContractAddress: "", dexterExchangeAddress: "")
		usdtz.setBalance(fromRpcAmount: "290000000")
		
		print("1.1: \(xtz.description)")
		print("1.2: \(xtz.rpcRepresentation)")
		print("1.3: \(xtz.normalisedRepresentation)")
		print("1.4: \(xtz.formatNormalisedRepresentation(locale: Locale(identifier: "en-us")) ?? "error")")
		print("1.5: \(xtz.formatNormalisedRepresentation(locale: Locale(identifier: "ru_MD")) ?? "error")")
		print("1.6: \(xtz.toRpcDecimal() ?? -1)")
		print("1.7: \(xtz.toNormalisedDecimal() ?? -1)")
		
		
		print("\n\n")
		xtz2.setBalance(fromRpcAmount: "1000000") // 1 XTZ
		
		print("2.1: \(xtz2.description)")
		print("2.2: \(xtz2.rpcRepresentation)")
		print("2.3: \(xtz2.normalisedRepresentation)")
		print("2.4: \(xtz2.formatNormalisedRepresentation(locale: Locale(identifier: "en-us")) ?? "error")")
		print("2.5: \(xtz2.formatNormalisedRepresentation(locale: Locale(identifier: "ru_MD")) ?? "error")")
		print("2.6: \(xtz2.toRpcDecimal() ?? -1)")
		print("2.7: \(xtz2.toNormalisedDecimal() ?? -1)")
		
		
		print("\n\n")
		xtz3.setBalance(fromNormalisedAmount: 1.47)
		
		print("3.1: \(xtz3.description)")
		print("3.2: \(xtz3.rpcRepresentation)")
		print("3.3: \(xtz3.normalisedRepresentation)")
		print("3.4: \(xtz3.formatNormalisedRepresentation(locale: Locale(identifier: "en-us")) ?? "error")")
		print("3.5: \(xtz3.formatNormalisedRepresentation(locale: Locale(identifier: "ru_MD")) ?? "error")")
		print("3.6: \(xtz3.toRpcDecimal() ?? -1)")
		print("3.7: \(xtz3.toNormalisedDecimal() ?? -1)")
		
		
		print("\n\n")
		let addedTogether = (xtz + xtz2 + xtz3)
		print("added together: \(addedTogether.toNormalisedDecimal() ?? -1)")
		
		
		print("\n\n")
		let multiplied = addedTogether * 0.42
		print("Multiplied by a dollar value: \(multiplied.description)") // 1.0374
		
		
		
		print("\n\n")
		usdtz.setBalance(fromRpcAmount: "290000000")
		
		print("4.1: \(usdtz.description)")
		print("4.2: \(usdtz.rpcRepresentation)")
		print("4.3: \(usdtz.normalisedRepresentation)")
		print("4.4: \(usdtz.formatNormalisedRepresentation(locale: Locale(identifier: "en-us")) ?? "error")")
		print("4.5: \(usdtz.formatNormalisedRepresentation(locale: Locale(identifier: "ru_MD")) ?? "error")")
		print("4.6: \(usdtz.toRpcDecimal() ?? -1)")
		print("4.7: \(usdtz.toNormalisedDecimal() ?? -1)")
		*/
	}
}

