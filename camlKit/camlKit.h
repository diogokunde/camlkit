//
//  camlKit.h
//  camlKit
//
//  Created by Simon Mcloughlin on 18/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for camlKit.
FOUNDATION_EXPORT double camlKitVersionNumber;

//! Project version string for camlKit.
FOUNDATION_EXPORT const unsigned char camlKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <camlKit/PublicHeader.h>


