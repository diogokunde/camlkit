Pod::Spec.new do |s|
	s.name             = 'camlKit'
	s.version          = '0.1.0'
	s.summary          = 'camlKit is a native Swift library for interacting with the Tezos Blockchain and other camlCase applications, such as Dexter.'
	s.description      = <<-DESC
	camlKit is a native Swift library, built by camlCase, to make interacting with the Tezos Blockchain easier. camlKit also includes APIs to help developers integrate other camlCase applications, such as Dexter.
						 DESC
	s.homepage         = 'https://gitlab.com/camlcase-dev/camlkit'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'Simon McLoughlin' => 'simon.d.mcl@gmail.com' }
	s.source           = { :git => 'https://gitlab.com/camlcase-dev/camlkit', :tag => s.version.to_s }
	s.ios.deployment_target = '12.0'
	s.osx.deployment_target = '10.15'
	s.swift_version = ['5.0', '5.1', '5.2']
	s.source_files = 'Sources/**/*'
	# s.frameworks = 'UIKit', 'MapKit'
	s.dependency 'BigInt', '5.0.0'
	s.dependency 'Base58Swift', '2.1.10'
	s.dependency 'MnemonicKit', '1.3.12'
	s.dependency 'Sodium', '0.8.0'
	s.dependency 'CryptoSwift', '1.3.1'
	s.dependency 'secp256k1.swift', '0.1.4'
	s.dependency 'KeychainSwift', '19.0'
  end