$minSupportedOS = '12.0'

platform :ios, $minSupportedOS

use_frameworks!
inhibit_all_warnings!

def shared_pods
  pod 'BigInt', '5.0.0'
	pod 'Base58Swift', '2.1.10'
	pod 'MnemonicKit', '1.3.12'
	pod 'Sodium', '0.8.0'
	pod 'CryptoSwift', '1.3.1'
	pod 'secp256k1.swift', '0.1.4'
	pod 'KeychainSwift', '19.0'
end

target 'camlKit' do
  shared_pods
end

target 'camlKit-Example' do
  shared_pods
end


# Hide all warnings from cocoapods
post_install do |installer|

  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
        config.build_settings['GCC_WARN_INHIBIT_ALL_WARNINGS'] = "YES"

        # Stop a app store connect error "Too many symbols"
        config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = $minSupportedOS
    end
  end

  # Pod-Framework
  installer.pods_project.targets.each do |target|
      if target.name == 'Pod-Framework'
          target.build_configurations.each do |config|
              config.build_settings['WARNING_CFLAGS'] ||= ['"-Wno-nullability-completeness"']
          end
      end
  end

  # This removes the warning about swift conversion, hopefuly forever!
  installer.pods_project.root_object.attributes['LastSwiftMigration'] = 9999
  installer.pods_project.root_object.attributes['LastSwiftUpdateCheck'] = 9999
  installer.pods_project.root_object.attributes['LastUpgradeCheck'] = 9999
end
