//
//  OSLogs+categories.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 17/08/2020.
//

import Foundation
import os.log

/// Extension to OSLog to create some custom categories for logging
extension OSLog {
	private static var subsystem = Bundle.main.bundleIdentifier ?? "io.camlcase.camlkit"
	
	static let camlKit = OSLog(subsystem: subsystem, category: "camlKit")
	static let keychain = OSLog(subsystem: subsystem, category: "camlKit-keychain")
	static let network = OSLog(subsystem: subsystem, category: "camlKit-network")
}
