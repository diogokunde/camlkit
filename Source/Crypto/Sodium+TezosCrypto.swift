//
//  Sodium+TezosCrypto.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 17/08/2020.
//

import Foundation
import Sodium

extension Sodium {
	
	public static let shared = Sodium()
}
