//
//  MichelsonFactory.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 18/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

/// Not implmented yet
public class MichelsonFactory {
	
	// Experiment to see can we incorporate this: https://github.com/ecadlabs/taquito/tree/master/packages/taquito-michel-codec instead of implementing the Michelson spec
	// Otherwise, for now it might be easier to do something similar to the smart contract code in MagmaWallet, where we have a class function returning the hardcoded json
}
