//
//  TezosNodeClient.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 18/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log


/// The TezosNodeClient offers methods for interacting with the Tezos node to fetch balances, send transactions etc.
/// The client will abstract away all the compelx tasks of remote forging, parsing, signing, preapply and injecting operations.
/// It will also convert amounts from the network into `Token` objects to make common tasks easier.
public class TezosNodeClient {
	
	/// Errors that the TezosNodeClient is capable of returning
	public enum TezosNodeClientError: Error {
		case unableToSetupForge
		case unableToSetupParse
		case parseFailed
		case signingFailure
		case unableToSetupPreapply
		case preapplyContainedError(errors: [OperationResponseInternalResultError]?)
		case unableToSetupInject
	}
	
	
	// MARK: - Private Properties
	
	private let metadataQueue: DispatchQueue
	private let forgeQueue: DispatchQueue
	
	
	
	// MARK: - Public Properties
	
	/// The configuration object containing all the necessary settings to connect and communicate with the Tezos node
	public let config: TezosNodeClientConfig
	
	/// The `NetworkService` object that will perform all the networking calls
	public let networkService: NetworkService
	
	/// The service responsible for calculating network fees on behalf of the user
	public let feeEstimatorService: FeeEstimatorService
	
	
	// MARK: - Init
	
	/**
	Init a `TezosNodeClient` with a `TezosNodeClientConfig`.
	- parameter config: A configuration object containing all the necessary settings to connect and communicate with the Tezos node.
	*/
	public init(config: TezosNodeClientConfig = TezosNodeClientConfig(withDefaultsForNetworkType: .mainnet)) {
		self.metadataQueue = DispatchQueue(label: "TezosNodeClient.metadata", qos: .background, attributes: [], autoreleaseFrequency: .inherit, target: nil)
		self.forgeQueue = DispatchQueue(label: "TezosNodeClient.forge", qos: .background, attributes: [], autoreleaseFrequency: .inherit, target: nil)
		self.config = config
		self.networkService = NetworkService(urlSession: config.urlSession, loggingConfig: config.loggingConfig)
		self.feeEstimatorService = FeeEstimatorService(tezosNodeClient: nil, networkService: self.networkService)
		
		// TODO: Split out the forge, sign, preapply ... functions into another class that can be used by TezosNodeClient and FeeEstimator
		self.feeEstimatorService.tezosNodeClient = self // can't pass in `self` until all properties set
	}
	
	
	
	// MARK: - Balance
	
	/**
	Gets the balance for a given Address.
	- parameter forAddress: A Tezos network address, starting with `"tz1"`, `"tz2"`, `"tz3"` or `"kt1"`
	- parameter completion: A callback containing a new `Token` object matching the xtz standard, or an error.
	- returns: Void
	*/
	public func getBalance(forAddress address: String, completion: @escaping ((Result<XTZAmount, Error>) -> Void)) {
		self.networkService.send(rpc: RPC.xtzBalance(forAddress: address), withBaseURL: config.primaryNodeURL) { (result) in
			switch result {
				case .success(let rpcAmount):
					let xtz = XTZAmount(fromRpcAmount: rpcAmount) ?? XTZAmount.zero()
					completion(Result.success(xtz))
				
				case .failure(let rpcError):
					completion(Result.failure(rpcError))
			}
		}
	}
	
	/// Not implmented yet
	public func getBalance(forTokens tokens: inout [Token], forAddress Address: String) {
		// Get balances for an array of tokens
		// Bake in the weird FA1.2 balance query
		// only do the meta data query stuff once for all the tokens
		// ... maybe include Messari directly in the SDK and mutate the objects passed in to contain the updated balances and prices
	}
	
	
	// MARK: - Delegate
	
	/**
	Gets the delegate for the given address.
	- parameter forAddress: A Tezos network address, starting with `"tz1"`, `"tz2"`, `"tz3"` or `"kt1"`
	- parameter completion: A callback containing a String with the delegate/baker's address, or an error.
	- returns: Void
	*/
	public func getDelegate(forAddress address: String, completion: @escaping ((Result<String, Error>) -> Void)) {
		self.networkService.send(rpc: RPC.getDelegate(forAddress: address), withBaseURL: config.primaryNodeURL, completion: completion)
	}
	
	/// Not implmented yet
	public func setDelegate(forWallet wallet: Wallet) {
		
	}
	
	
	// MARK: Estimate
	
	/// Not implmented yet
	public func estimate(operations: [Operation], withWallet wallet: Wallet, completion: @escaping ((Result<[Operation], Error>) -> Void)) {
		
		getOperationMetadata(forWallet: wallet) { [weak self] (result) in
			
			switch result {
				case .success(let metadata):
					self?.feeEstimatorService.estimate(operations: operations, operationMetadata: metadata, withWallet: wallet, completion: completion)
				
				case .failure(let error):
					os_log(.error, log: .camlKit, "Unable to fetch metadata: %@", "\(error)")
					completion(Result.failure(error))
			}
		}
	}
	
	
	// MARK: - Send
	
	/**
	Send an array of `Operation`'s to the blockchain. Use `OperationFactory` to help create this array for common use cases.
	- parameter operations: An array of `Operation` subclasses to be sent to the network.
	- parameter withWallet: The `Wallet` instance that will sign the transactions.
	- parameter completion: A completion closure that will either return the opertionID of an injected operation, or an error.
	*/
	public func send(operations: [Operation], withWallet wallet: Wallet, completion: @escaping ((Result<String, Error>) -> Void)) {
		
		getOperationMetadata(forWallet: wallet) { [weak self] (result) in
			
			switch result {
				case .success(let metadata):
					let operationPayload = OperationFactory.operationPayload(fromMetadata: metadata, andOperations: operations, withWallet: wallet)
					self?.send(operationPayload: operationPayload, operationMetadata: metadata, withWallet: wallet, completion: completion)
				
				case .failure(let error):
					os_log(.error, log: .camlKit, "Unable to fetch metadata: %@", "\(error)")
					completion(Result.failure(error))
			}
		}
	}
	
	/**
	Send an already contrsutructed `OperationPayload` with the necessary `OperationMetadata` without having to fetch metadata again.
	- parameter operationPayload: An `OperationPayload` that has already been constructed (e.g. from the estimation call).
	- parameter operationMetadata: An `OperationMetaData` object containing all the info about the network that the call needs for forge -> inject.
	- parameter withWallet: The `Wallet` instance that will sign the transactions.
	- parameter completion: A completion closure that will either return the opertionID of an injected operation, or an error.
	*/
	public func send(operationPayload: OperationPayload, operationMetadata: OperationMetadata, withWallet wallet: Wallet, completion: @escaping ((Result<String, Error>) -> Void)) {
		self.forgeParseSignPreapplyInject(operationMetadata: operationMetadata, operationPayload: operationPayload, wallet: wallet, completion: completion)
	}
	
	
	
	// MARK: - Blockchain Operations
	
	/**
	Get all the metadata necessary from the network to perform operations.
	- parameter forWallet: The `Wallet` object that will be sending the operations.
	- parameter completion: A callback that will be executed when the network requests finish.
	*/
	public func getOperationMetadata(forWallet wallet: Wallet, completion: @escaping ((Result<OperationMetadata, Error>) -> Void)) {
		let dispatchGroup = DispatchGroup()
		
		var counter = 0
		var managerKey = ""
		var blockchainHead = BlockchainHead(protocol: "", chainID: "", hash: "")
		var error: Error? = nil
		
		
		// Get manager key
		dispatchGroup.enter()
		metadataQueue.async { [weak self] in
			if let url = self?.config.primaryNodeURL {
				self?.networkService.send(rpc: RPC.managerKey(forAddress: wallet.address), withBaseURL: url) { (result) in
					switch result {
						case .success(let value):
							managerKey = value
						
						case .failure(let err):
							error = err
					}
					
					dispatchGroup.leave()
				}
			} else {
				error = NetworkService.NetworkError.invalidURL
				dispatchGroup.leave()
			}
		}
		
		
		// Get counter
		dispatchGroup.enter()
		metadataQueue.async { [weak self] in
			if let url = self?.config.primaryNodeURL {
				self?.networkService.send(rpc: RPC.counter(forAddress: wallet.address), withBaseURL: url) { (result) in
					switch result {
						case .success(let value):
							counter = Int(value) ?? 0
						
						case .failure(let err):
							error = err
					}
					
					dispatchGroup.leave()
				}
			} else {
				error = NetworkService.NetworkError.invalidURL
				dispatchGroup.leave()
			}
		}
		
		
		// Get blockchain head
		dispatchGroup.enter()
		metadataQueue.async { [weak self] in
			if let url = self?.config.primaryNodeURL {
				self?.networkService.send(rpc: RPC.blockchainHead(), withBaseURL: url) { (result) in
					switch result {
						case .success(let value):
							blockchainHead = value
						
						case .failure(let err):
							error = err
					}
					
					dispatchGroup.leave()
				}
			} else {
				error = NetworkService.NetworkError.invalidURL
				dispatchGroup.leave()
			}
		}
		
		
		// When all requests finished, return on main thread
		dispatchGroup.notify(queue: .main) {
			if let err = error {
				completion(Result.failure(err))
				
			} else {
				completion(Result.success(OperationMetadata(managerKey: managerKey, counter: counter, blockchainHead: blockchainHead)))
			}
		}
	}
	
	/**
	Every `Operation` needs to be Forged, Parsed, Signed, Preapply'd and Injected to make its way into the blockchain.
	This function will complete all of those steps and return an OperationID or an Error.
	- parameter operationMetadata:
	- parameter operationPayload:
	- parameter wallet:
	- parameter completion:
	*/
	public func forgeParseSignPreapplyInject(operationMetadata: OperationMetadata, operationPayload: OperationPayload, wallet: Wallet, completion: @escaping ((Result<String, Error>) -> Void)) {
		var forgedHash = ""
		
		// Forge the operations remotely
		forgeQueue.async { [weak self] in
			
			// Perform a remote forge of the oepration with the metadata
			self?.forge(operationMetadata: operationMetadata, operationPayload: operationPayload, wallet: wallet, completion: { [weak self] (result) in
				
				// Parse remotely against a different server to verify hash is correct before continuing
				self?.parse(forgeResult: result, operationMetadata: operationMetadata, operationPayload: operationPayload) { (innerResult) in
					switch innerResult {
						case .success(let hash):
							os_log(.debug, log: .camlKit, "Remote parse successful")
							forgedHash = hash
						
						case .failure(let parseError):
							completion(Result.failure(parseError))
							return
					}
					
					// With a successful Parse, we can continue on to Sign, Preapply (to check for errors) and if no errors, inject the operation
					self?.signPreapplyAndInject(wallet: wallet, forgedHash: forgedHash, operationPayload: operationPayload, operationMetadata: operationMetadata, completion: completion)
				}
			})
		}
	}
	
	/// Internal function to group together operations for readability sake
	private func signPreapplyAndInject(wallet: Wallet, forgedHash: String, operationPayload: OperationPayload, operationMetadata: OperationMetadata, completion: @escaping ((Result<String, Error>) -> Void)) {
		
		// Sign the forged hash
		guard let signature = wallet.sign(forgedHash),
			let signatureHex = CryptoUtils.binToHex(signature) else {
			completion(Result.failure(TezosNodeClientError.signingFailure))
			return
		}
		
		
		// Add the signature and protocol to the payload
		var signedPayload = operationPayload
		signedPayload.addSignature(signature, signingCurve: wallet.secretKey.signingCurve)
		signedPayload.addProtcol(fromMetadata: operationMetadata)
		
		
		// Perform the preapply to check for errors, otherwise attempt to inject the operation onto the blockchain
		self.preapply(operationMetadata: operationMetadata, operationPayload: signedPayload) { [weak self] (preapplyResult) in
			self?.inject(signedBytes: forgedHash+signatureHex, handlePreapplyResult: preapplyResult) { (injectResult) in
				completion(injectResult)
			}
		}
	}
	
	/**
	Forge an `OperationPayload` remotely, so it can be sent to the RPC.
	- parameter operationMetadata: fetched from `getOperationMetadata(...)`.
	- parameter operationPayload: created from `OperationFactory.operationPayload()`.
	- parameter wallet: The `Wallet` object that will sign the operations.
	- parameter completion: callback with a forged hash or an error.
	*/
	public func forge(operationMetadata: OperationMetadata, operationPayload: OperationPayload, wallet: Wallet, completion: @escaping ((Result<String, Error>) -> Void)) {
		
		guard let rpc = RPC.forge(operationPayload: operationPayload, withMetadata: operationMetadata) else {
			os_log(.error, log: .camlKit, "Unable to create forge RPC, cancelling event")
			completion(Result.failure(TezosNodeClientError.unableToSetupForge))
			return
		}
		
		self.networkService.send(rpc: rpc, withBaseURL: config.primaryNodeURL) { (result) in
			switch result {
				case .success(let string):
					completion(Result.success(string))
					
				case .failure(let error):
					os_log(.error, log: .camlKit, "Unable to remote forge: %@", "\(error)")
					completion(Result.failure(error))
			}
		}
	}
	
	/**
	Parse a forged `OperationPayload` on a different server to ensure nobody maliciously tampared with the request.
	- parameter forgeResult: The `Result` object from the `forge(...)` function.
	- parameter operationMetadata: fetched from `getOperationMetadata(...)`.
	- parameter operationPayload: the `OperationPayload` to compare against to ensure it matches.
	- parameter completion: callback which just returns success or failure with an error.
	*/
	public func parse(forgeResult: Result<String, Error>, operationMetadata: OperationMetadata, operationPayload: OperationPayload, completion: @escaping ((Result<String, Error>) -> Void)) {
		
		// Handle the forge result first to check there are no errors
		var remoteForgedHash = ""
		
		switch forgeResult {
			case .success(let forgedHash):
				remoteForgedHash = forgedHash
			
			case .failure(_):
				completion(forgeResult) // Pass error upwards and exit early
				return
		}
		
		
		// Continue with parse
		guard let rpc = RPC.parse(hashToParse: remoteForgedHash, metadata: operationMetadata) else {
			os_log(.error, log: .camlKit, "Unable to create parse RPC, cancelling event")
			completion(Result.failure(TezosNodeClientError.unableToSetupParse))
			return
		}
		
		self.networkService.send(rpc: rpc, withBaseURL: config.parseNodeURL) { (result) in
			switch result {
				case .success(let parsedPayload):
					
					if parsedPayload.count > 0 && parsedPayload[0] == operationPayload {
						completion(Result.success(remoteForgedHash))
					} else {
						print("parsedPayload.count > 0: \(parsedPayload.count > 0)")
						print("parsedPayload[0] == operationPayload : \(parsedPayload[0] == operationPayload)")
						completion(Result.failure(TezosNodeClientError.parseFailed))
					}
					
				case .failure(let error):
					os_log(.error, log: .camlKit, "Unable to remote forge: %@", "\(error)")
					completion(Result.failure(error))
			}
		}
	}
	
	/**
	Preapply a signed `OperationPayload` to check for any errors.
	- parameter operationMetadata: Fetched from `getOperationMetadata(...)`.
	- parameter operationPayload: An `OperationPayload`that has had a signature and a protcol added to it.
	- parameter completion: Callback which just returns success or failure with an error.
	*/
	public func preapply(operationMetadata: OperationMetadata, operationPayload: OperationPayload, completion: @escaping ((Result<[OperationResponse], Error>) -> Void)) {
		
		guard let rpc = RPC.preapply(operationPayload: operationPayload, withMetadata: operationMetadata) else {
			os_log(.error, log: .camlKit, "Unable to create preapply RPC, cancelling event")
			completion(Result.failure(TezosNodeClientError.unableToSetupPreapply))
			return
		}
		
		self.networkService.send(rpc: rpc, withBaseURL: config.primaryNodeURL) { (result) in
			switch result {
				case .success(let operationResponse):
					completion(Result.success(operationResponse))
					
				case .failure(let error):
					os_log(.error, log: .camlKit, "Unable to remote forge: %@", "\(error)")
					completion(Result.failure(error))
			}
		}
	}
	
	
	/**
	Inject a signed bytes to become part of the next block on the blockchain
	- parameter signedBytes: The result of the forge operation (as a string) with the signature (as a hex string) appended on.
	- parameter handlePreapplyResult: Optionally pass in the result of the preapply function to reduce the indentation required to perform the full set of operations. Any error will be returned via the injection Result object.
	*/
	public func inject(signedBytes: String, handlePreapplyResult: Result<[OperationResponse], Error>?, completion: @escaping ((Result<String, Error>) -> Void)) {
		
		// Check if we are handling a preapply result to check for errors
		if let preapplyResult = handlePreapplyResult {
			switch preapplyResult {
				case .success(let operationResponse):
					
					// Search for errors inside each `OperationResponse`, if theres an error present return them all
					let errors = operationResponse.compactMap({ $0.errors() }).reduce([], +)
					if errors.count > 0 {
						completion(Result.failure(TezosNodeClientError.preapplyContainedError(errors: errors)))
						return
					}
					
					// Else continue on to inject. The purpose of the preapply is to just check for errors. If no errors, we can safely attempt an injection
				
				case .failure(let preapplyError):
					completion(Result.failure(preapplyError))
					return
			}
		}
		
		// Continue on with the injection
		guard let rpc = RPC.inject(signedBytes: signedBytes) else {
			os_log(.error, log: .camlKit, "Unable to create inject RPC, cancelling event")
			completion(Result.failure(TezosNodeClientError.unableToSetupInject))
			return
		}
		
		self.networkService.send(rpc: rpc, withBaseURL: config.primaryNodeURL) { (result) in
			completion(result)
		}
	}
}
