//
//  TezosNodeClientConfig.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 19/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation

/// A configuration object used to provide settings to the TezosNodeClient
public struct TezosNodeClientConfig {
	
	// MARK: - Types
	
	/// An enum indicating whether the network is mainnet or testnet
	public enum NetworkType {
		case mainnet
		case testnet
	}
	
	
	
	// MARK: - Public Constants
	
	/// The default mainnet URL to use for `primaryNodeURL`
	/// For more information on the free service, see: https://tezos.giganode.io/
	public static let defaultPrimaryMainnetNodeURL = URL(string: "https://mainnet-tezos.giganode.io/")!
	
	/// The default testnet URL to use for `parseNodeURL`
	/// For more information on the free service, see: https://nautilus.cloud/
	public static let defaultParseMainnetNodeURL = URL(string: "https://tezos-prod.cryptonomic-infra.tech:443/")!
	
	/// The default testnet URL to use for `primaryNodeURL`
	/// For more information on the free service, see: https://tezos.giganode.io/
	public static let defaultPrimaryTestnetNodeURL = URL(string: "https://testnet-tezos.giganode.io/")!
	
	/// The default testnet URL to use for `parseNodeURL`
	/// For more information on the free service, see: https://nautilus.cloud/
	public static let defaultParseTestnetNodeURL = URL(string: "https://tezos-dev.cryptonomic-infra.tech:443/")!
	
	
	
	// MARK: - Public Properties
	
	/// The main URL used for remote forging, fetching balances, setting delegates and other forms of queries and operations.
	public let primaryNodeURL: URL
	
	/// When using remote forging, it is essential to use a second server to verify the contents of the remote forge match what the library sent.
	public let parseNodeURL: URL
	
	/// The `URLSession` that will be used for all network communication. If looking to mock this library, users should create their own `URLSessionMock` and pass it in.
	public let urlSession: URLSession
	
	/// The network type of the connected node
	public let networkType: NetworkType
	
	/// Control what gets logged to the console
	public var loggingConfig: LoggingConfig = LoggingConfig()
	
	
	
	// MARK: - Init
	
	/**
	Init a `TezosNodeClientConfig` with your own settings
	- parameter primaryNodeURL: The URL of the primary node that will perform the majority of the network operations.
	- parameter parseNodeURL: The URL to use to parse and verify a remote forge.
	- parameter urlSession: The URLSession object that will perform all the network operations.
	- parameter networkType: Enum indicating the network type.
	*/
	public init(primaryNodeURL: URL, parseNodeURL: URL, urlSession: URLSession, networkType: NetworkType) {
		self.primaryNodeURL = primaryNodeURL
		self.parseNodeURL = primaryNodeURL
		self.urlSession = urlSession
		self.networkType = networkType
	}
	
	/**
	Init a `TezosNodeClientConfig` with the defaults
	- parameter withDefaultsForNetworkType: Use the default settings for the given network type
	*/
	public init(withDefaultsForNetworkType networkType: NetworkType) {
		
		self.urlSession = URLSession.shared
		self.networkType = networkType
		
		switch networkType {
			case .mainnet:
				primaryNodeURL = TezosNodeClientConfig.defaultPrimaryMainnetNodeURL
				parseNodeURL = TezosNodeClientConfig.defaultParseMainnetNodeURL
			
			case .testnet:
				primaryNodeURL = TezosNodeClientConfig.defaultPrimaryTestnetNodeURL
				parseNodeURL = TezosNodeClientConfig.defaultParseTestnetNodeURL
		}
	}
}
