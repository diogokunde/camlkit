//
//  TokenAmount.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 20/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import BigInt
import os.log

/// Class representing a numeric amount on the Tezos network. The network uses natural numbers inside strings, which technically have an infinite length.
/// This class is used to encapsulate a `BigInt` and provide all the necessary init's and formatting functions to work with the networks requirements.
public class TokenAmount {
	
	/// The number of decimal places that this token supports.
	public let decimalPlaces: Int
	
	/// The balance of the token will be stored in the format expected by the RPC, and converted when needed to the display format users expect.
	/// For example: XTZ has 6 decimal places. But the decimal places are merely used for displaying to the end user. On the network 1 XTZ = 1000000, and is formatted as1.000000 in the client.
	///
	/// Intentiaonlly chose to not use unsigned BigInt. While its true that tokens on the Tezos network can't be negative (not such thing as holding -1 XTZ),
	/// there are situations such as transaction history display, or computing fees when sending the maximum balance, where it makes sense to allow the class to hold a negative value.
	/// It should be the responsibility of the `TezosNodeClient` or the `RPC` layer or the `NetworkService` to validate that it is a valid amount to send.
	private var internalBigInt: BigInt = 0
	
	/// Format the internal value to ensure it matches the format the RPC will expect
	public var rpcRepresentation: String {
		
		// Trim leading Zero's
		let intermediateString = String(internalBigInt)
		let santizedString = intermediateString.replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
		
		// When implementing the RPC parse function, returning an empty string causes mismatches.
		// The Tezos node will replace empty strings with "0", as it always expects a value to be present
		if santizedString == "" {
			return "0"
		}
		
		return santizedString
	}
	
	/// Basic formatting of a token to be human readable. For more advanced options, use the format function
	public var normalisedRepresentation: String {
		// Pad the value so its at least `decimalPlaces` long
		var paddedDecimalAmount = String(internalBigInt)
		while paddedDecimalAmount.count < decimalPlaces {
		  paddedDecimalAmount = "0" + paddedDecimalAmount
		}

		let decimalAmount = paddedDecimalAmount.suffix(decimalPlaces)

		let integerAmountLength = paddedDecimalAmount.count - decimalPlaces
		let integerAmount = integerAmountLength != 0 ? paddedDecimalAmount.prefix(integerAmountLength) : "0"
		return integerAmount + "." + decimalAmount
	}
	
	
	// MARK: - Init
	
	/**
	Set the internal balance, using a RPC string (most likely directly from the RPC node response).  e.g. "1 XTZ"  to the user = "1000000" to the RPC, as there are no such thing as decimal places on the network
	- parameter fromRpcAmount: A string conforming to the RPC standard for the given token.
	*/
	public init?(fromRpcAmount rpcAmount: String, decimalPlaces: Int) {
		guard CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: rpcAmount)) else {
			os_log(.error, log: .camlKit, "Can't set balance on a Token of tokenType.empty, or pass in a string with non digit characters")
			return nil
		}
		
		self.decimalPlaces = decimalPlaces
		self.internalBigInt = BigInt(rpcAmount) ?? 0
	}
	
	/**
	Set the internal balance, using a decimal version of an RPC amount.  e.g. "1 XTZ"  to the user = "1000000" to the RPC, as there are no such thing as decimal places on the network
	- parameter fromRpcAmount: A decimal conforming to the RPC standard for the given token. Decimal places will be ignored.
	*/
	public convenience init?(fromRpcAmount rpcAmount: Decimal, decimalPlaces: Int) {
		self.init(fromRpcAmount: rpcAmount.description, decimalPlaces: decimalPlaces)
	}
	
	/**
	Set the internal balance, using a decimal version of a normalised amount. e.g. if the amount is 1.5 and the token is xtz, internally it will be stored as 1500000
	- parameter fromNormalisedAmount: A decimal containing an amount for the given token. Anything over the given decimal places for the token will be ignored.
	*/
	public init(fromNormalisedAmount normalisedAmount: Decimal, decimalPlaces: Int) {
		let integerValue = BigInt(normalisedAmount.description) ?? 0

		// Convert decimalPlaces significant digits of decimals into integers to avoid having to deal with decimals.
		let multiplierDoubleValue = (pow(10, decimalPlaces) as NSDecimalNumber).doubleValue
		let multiplierIntValue = (pow(10, decimalPlaces) as NSDecimalNumber).intValue
		let significantDecimalDigitsAsInteger = BigInt((normalisedAmount * Decimal(multiplierDoubleValue)).description) ?? 0
		let significantIntegerDigitsAsInteger = BigInt(integerValue * BigInt(multiplierIntValue))
		let decimalValue = significantDecimalDigitsAsInteger - significantIntegerDigitsAsInteger
		
		self.decimalPlaces = decimalPlaces
		self.internalBigInt = (integerValue * BigInt(10).power(decimalPlaces)) + decimalValue
	}
	
	/**
	Set the internal balance, using a normalised amount string. e.g. if the amount is 1.5 and the token is xtz, internally it will be stored as 1500000
	- parameter fromNormalisedAmount: A string containing an amount for the given token. Anything over the given decimal places for the token will be ignored.
	*/
	public convenience init?(fromNormalisedAmount normalisedAmount: String, decimalPlaces: Int) {
		guard let decimal = Decimal(string: normalisedAmount) else {
			os_log(.error, log: .camlKit, "Can't set balance as can't parse string")
			return nil
		}
		
		self.init(fromNormalisedAmount: decimal, decimalPlaces: decimalPlaces)
	}
	
	/**
	Private init to create an object with a `BigInt`. Used as an internal helper, should not be used by develoeprs using camlKit.
	*/
	private init(decimalPlaces: Int) {
		self.decimalPlaces = decimalPlaces
		self.internalBigInt = 0
	}
	
	/**
	Private init to create an object with a `BigInt`. Used as an internal helper, should not be used by develoeprs using camlKit.
	*/
	private init(bigInt: BigInt, decimalPlaces: Int) {
		self.decimalPlaces = decimalPlaces
		self.internalBigInt = bigInt
	}
	
	/**
	Quickly create a `TokenAmount` with zero balance.
	*/
	public static func zeroBalance(decimalPlaces: Int) -> TokenAmount {
		return TokenAmount(decimalPlaces: decimalPlaces)
	}
	
	
	
	// MARK: - Display
	
	/**
	Format the current value into a human readable string, using the given locale.
	- parameter locale: The locale to use to decide whether to use decimal or comma, comma or spaces etc, when formattting the number
	*/
	public func formatNormalisedRepresentation(locale: Locale) -> String? {
		guard let decimal = self.toNormalisedDecimal() else {
			return nil
		}
		
		let numberFormatter = NumberFormatter()
		numberFormatter.locale = locale
		numberFormatter.numberStyle = .decimal
		numberFormatter.maximumFractionDigits = decimalPlaces
		
		return numberFormatter.string(from: decimal as NSNumber)
	}
	
	/**
	Function to convert the underlying rpc value into a `Decimal` which can be useful in some situations for integrating with other tools and frameworks.
	**Warning** `Decimal` has a limited, lower treshold (163 digits). Its possible it can overrun, hence the optional return value.
	*/
	public func toRpcDecimal() -> Decimal? {
		guard let decimal = Decimal(string: internalBigInt.description)?.rounded(scale: decimalPlaces, roundingMode: .down) else {
			return nil
		}
		
		return decimal
	}
	
	/**
	Function to convert the underlying normalised value into a `Decimal` which can be useful in some situations for integrating with other tools and frameworks.
	**Warning** `Decimal` has a limited, lower treshold (163 digits). Its possible it can overrun, hence the optional return value.
	*/
	public func toNormalisedDecimal() -> Decimal? {
		guard let decimal = Decimal(string: internalBigInt.description) else {
			return nil
		}
		
		return (decimal / pow(10, decimalPlaces)).rounded(scale: decimalPlaces, roundingMode: .down)
	}
	
	
	
	// MARK: - Arithmetic
	
	/// Function to check if tokens have the same decimal palces, and log an error if not. Not supporting the idea of adding unrelated tokens together
	private static func sameTokenCheck(lhs: TokenAmount, rhs: TokenAmount) -> Bool {
		let result = (lhs.decimalPlaces == rhs.decimalPlaces)
		
		if !result {
			os_log(.error, log: .camlKit, "Arithmetic function is not possible between tokens with different decimal places (%@ and %@). Ignoring operation.", lhs.decimalPlaces, rhs.decimalPlaces)
		}
		
		return result
	}
	
	/**
	Overload + operator to allow users to add two `Token` amounts of the same type, together.
	*/
	public static func + (lhs: TokenAmount, rhs: TokenAmount) -> TokenAmount {
		guard sameTokenCheck(lhs: lhs, rhs: rhs) else {
			return TokenAmount.zeroBalance(decimalPlaces: lhs.decimalPlaces)
		}
		
		return TokenAmount(bigInt: (lhs.internalBigInt + rhs.internalBigInt), decimalPlaces: lhs.decimalPlaces)
	}
	
	/**
	Overload += operator to allow users to add two `Token` amounts of the same type, together in place.
	*/
	public static func += (lhs: inout TokenAmount, rhs: TokenAmount) {
		guard sameTokenCheck(lhs: lhs, rhs: rhs) else {
			return
		}
		
		let result = lhs + rhs
		lhs = result
	}
	
	/**
	Overload - operator to allow users to subtract two `Token` amounts of the same type.
	*/
	public static func - (lhs: TokenAmount, rhs: TokenAmount) -> TokenAmount {
		guard sameTokenCheck(lhs: lhs, rhs: rhs) else {
			return TokenAmount.zeroBalance(decimalPlaces: lhs.decimalPlaces)
		}
		
		return TokenAmount(bigInt: (lhs.internalBigInt - rhs.internalBigInt), decimalPlaces: lhs.decimalPlaces)
	}
	
	/**
	Overload -= operator to allow users to subtract one `Token` amount of the same type from another, together in place.
	*/
	public static func -= (lhs: inout TokenAmount, rhs: TokenAmount) {
		guard sameTokenCheck(lhs: lhs, rhs: rhs) else {
			return
		}
		
		let result = lhs - rhs
		lhs = result
	}
	
	/**
	Overload multiplcation operator to allow users to multiple a token by a dollar value, and return the localCurrency value of the token.
	*/
	public static func * (lhs: TokenAmount, rhs: Decimal) -> Decimal {
		let lhsDecimal = lhs.toNormalisedDecimal() ?? 0
		return lhsDecimal * rhs
	}
	
	/**
	Overload multiplcation operator to allow users to multiple a token by an Int. Useful for fee caluclation
	*/
	public static func * (lhs: TokenAmount, rhs: Int) -> TokenAmount {
		return TokenAmount(bigInt: (lhs.internalBigInt * BigInt(clamping: rhs)), decimalPlaces: lhs.decimalPlaces)
	}
}



// MARK: - Extensions

extension TokenAmount: Comparable {
	
	/// Conforming to `Comparable`
	public static func < (lhs: TokenAmount, rhs: TokenAmount) -> Bool {
		return lhs.internalBigInt < rhs.internalBigInt
	}
}

extension TokenAmount: Equatable {
	
	/// Conforming to `Equatable`
	public static func == (lhs: TokenAmount, rhs: TokenAmount) -> Bool {
		return lhs.internalBigInt == rhs.internalBigInt
	}
}

extension TokenAmount: CustomStringConvertible {
	
	/// Conforming to `CustomStringConvertible` to print a number, giving the appearence of a numeric type
	public var description: String {
		return rpcRepresentation
	}
}
