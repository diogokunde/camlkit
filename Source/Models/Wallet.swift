//
//  Wallet.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 17/08/2020.
//

import Foundation
import MnemonicKit
import KeychainSwift
import os.log

/**
A Tezos Wallet used for signing transactions before sending to the Tezos network. This object holds the public and private key used to create the contained Tezos address.
You should **NOT** store a copy of this class in a singleton or gloabl variable of any kind. it should be created as needed and nil'd when not.
In order to help developers achieve this, the wallet object can automatically store the results of long running operations directly to the keychain.
The wallet can then be recovered in a fraction of the time by using `Wallet.restoreFromKeychain()`. Warning, each time its written to the keychain, it will overwrite any previous value.
For added security, the memory used by the keys will be scrubbed whenever the class is deinitialised.
*/
public class Wallet {
	
	// MARK: - Private properties
	
	private static let keychain = KeychainSwift(keyPrefix: "io.camlcase.camlkit.wallet.")
	
	private struct KeychainKeys {
		static let seedString = "seed.string"
		static let signingCurveKey = "curve"
		static let mnemonicKey = "mnemonic"
	}
	
	
	
	// MARK: - Public Properties
	
	/// Used for specifying the number of words inside a Mnemonic
	public enum MnemonicPhraseLength: Int {
		case twelve = 128
		case fifteen = 160
		case eighteen = 192
		case twentyOne = 224
		case twentyFour = 256
	}
	
	/// An object representing the PublicKey of the current wallet
	public var publicKey: PublicKey
	
	/// An object representing the SecretKey of the current wallet
	public var secretKey: SecretKey
	
	/// The tz1 / tz2 / tz3 network address used by the current wallet
	public let address: String
	
	/// A bip39 compliant mnemonic recovery phrase
	public let mnemonic: String?
	
	
	
	// MARK: - Init
	
	/**
	Manually create an instance of a `Wallet`. Recommended to use the `create()` functions.
	- parameter publicKey: The publicKey for the given wallet that will or is shared with the Tezos network.
	- parameter secretKey: The private secretKey used for signing operations.
	- parameter address: The public facing address of the account.
	- parameter mnemonic: The private mnemonic used to recover / recreate the secretKey.
	*/
	public init(publicKey: PublicKey, secretKey: SecretKey, address: String, mnemonic: String?) {
		self.publicKey = publicKey
		self.secretKey = secretKey
		self.address = address
		self.mnemonic = mnemonic
	}
	
	/// Zero scrub keys when object is released to prevent any attempt to inspect the memory from external tools
	deinit {
		publicKey.bytes = Array(repeating: 0, count: publicKey.bytes.count)
		secretKey.bytes = Array(repeating: 0, count: secretKey.bytes.count)
	}
	
	
	
	// MARK: - Create
	
	/**
	Used to create a wallet object by specifying the length on the mnemonic in words. Optionally set a passphrase and a signing curve. Function will automatically cache data in the keychain.
	- parameter withMnemonicOfLength: enum indicating the length of the mnemonic to generate.
	- parameter passphrase: Optional. A user entered passphrase, to be added to the mnemonic for extra security.
	- parameter signingCurve: Optional. The elliptical curve to use when creating the address.
	- parameter writeToKeychain: Whether or not to automatically write the necessary data to the keychain, so it can be recovery later. Warning: Will overwrite any old data.
	- parameter completion: A block that will be executed when the operation is finished.
	- returns: Void
	*/
	public static func create(withMnemonicOfLength mnemonicLength: MnemonicPhraseLength, passphrase: String = "", signingCurve: EllipticalCurve = .ed25519, writeToKeychain: Bool, completion: @escaping ((Wallet?) -> Void)) {
		guard let mnemonic = Mnemonic.generateMnemonic(strength: mnemonicLength.rawValue) else {
			os_log(.error, log: .camlKit, "Unable to generate Mnemonic")
			completion(nil)
			return
		}
		
		create(withMnemonic: mnemonic, passphrase: passphrase, signingCurve: signingCurve, writeToKeychain: writeToKeychain) { (wallet) in
			DispatchQueue.main.async { completion(wallet) }
		}
	}
	
	/**
	Used to create a wallet object by passing in a mnemonic string. Optionally set a passphrase and a signing curve. Function will automatically cache data in the keychain.
	- parameter withMnemonic: A string of words conforming to the bip39 standard.
	- parameter passphrase: Optional. A user entered passphrase, to be added to the mnemonic for extra security.
	- parameter signingCurve: Optional. The elliptical curve to use when creating the address.
	- parameter writeToKeychain: Whether or not to automatically write the necessary data to the keychain, so it can be recovery later. Warning: Will overwrite any old data.
	- parameter completion: A block that will be executed when the operation is finished.
	- returns: Void
	*/
	public static func create(withMnemonic mnemonic: String, passphrase: String = "", signingCurve: EllipticalCurve = .ed25519, writeToKeychain: Bool, completion: @escaping ((Wallet?) -> Void)) {
		
		// Put on background queue as the seedString generation makes many seconds (depending on device), and we don't want to block the main thread
		DispatchQueue.global(qos: .background).async {
			guard let seedString = Wallet.seedString(from: mnemonic, passphrase: passphrase),
				let secretKey = SecretKey(seedString: seedString, signingCurve: signingCurve),
				let publicKey = PublicKey(secretKey: secretKey),
				let address = publicKey.publicKeyHash else {
					
					os_log(.error, log: .camlKit, "Unable to create wallet data from mnemonic")
					DispatchQueue.main.async { completion(nil) }
					return
			}
			
			if writeToKeychain {
				// Store output from long running queries or unrecoverable from other sources, so Wallet object can be re-created quickly after onboarding
				let keychainResult1 = keychain.set(seedString, forKey: KeychainKeys.seedString, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
				let keychainResult2 = keychain.set(mnemonic, forKey: KeychainKeys.mnemonicKey, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
				let keychainResult3 = keychain.set(signingCurve.rawValue, forKey: KeychainKeys.signingCurveKey, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
			
				// Check for any errors and log
				if !keychainResult1 || !keychainResult2 || !keychainResult3 {
					os_log(.error, log: .keychain, "Unable to write wallet data to keychain. OSStatus: %@", keychain.lastResultCode)
				}
			}
			
			DispatchQueue.main.async { completion(Wallet(publicKey: publicKey, secretKey: secretKey, address: address, mnemonic: mnemonic)) }
		}
	}
	
	/**
	Quickly recover a wallet already stored on the system, avoiding long running crypto operations.
	- returns: `Wallet`
	*/
	public static func restoreFromKeychain() -> Wallet? {
		guard let seedString = keychain.get(KeychainKeys.seedString),
			let mnemonic = keychain.get(KeychainKeys.mnemonicKey),
			let signingCurve = keychain.get(KeychainKeys.signingCurveKey) else {
				
				os_log(.error, log: .keychain, "Unable to recover wallet from keychain. OSStatus: %@", keychain.lastResultCode)
				return nil
		}
		
		guard let secretKey = SecretKey(seedString: seedString, signingCurve: EllipticalCurve(rawValue: signingCurve) ?? EllipticalCurve.ed25519),
			let publicKey = PublicKey(secretKey: secretKey),
			let address = publicKey.publicKeyHash else {
				
				os_log(.error, log: .camlKit, "Unable to recovery wallet from keychain. Unable to parse data")
				return nil
		}
		
		return Wallet(publicKey: publicKey, secretKey: secretKey, address: address, mnemonic: mnemonic)
	}
	
	
	
	// MARK: - Helper Functions
	
	/**
	Fetch the mnemonic currently sotred in the keychain.
	- returns: A string containing a bip39 compliant mnemonic, or `nil`
	*/
	public func currentMnemonicStored() -> String? {
		guard let mnemonic = Wallet.keychain.get(KeychainKeys.mnemonicKey) else {
			return nil
		}
		
		return mnemonic
	}
	
	/**
	Used for signing operations before sending to the Tezos node.
	- parameter hex: A hex string to be signed by the secret key.
	- returns: An array of bytes.
	*/
	public func sign(_ hex: String) -> [UInt8]? {
		return secretKey.sign(hex: hex)
	}
	
	/**
	Generate a seed string from an mnemonic.
	- parameter from: A string matching the bip39 standard for a mnemonic.
	- parameter passphrase: Optional. A passphrase to be added to the mnemonic
	- returns: A seed string used to create a secret key.
	*/
	public static func seedString(from mnemonic: String, passphrase: String = "") -> String? {
		guard let rawSeedString = Mnemonic.deterministicSeedString(from: mnemonic, passphrase: passphrase) else {
			return nil
		}
		
		return String(rawSeedString[..<rawSeedString.index(rawSeedString.startIndex, offsetBy: 64)])
	}
	
	/**
	Validate a mnemonic string meets the bip39 stnadard.
	- parameter mnemonic: A string matching the bip39 standard for a mnemonic.
	- returns: A boolean indicating whether it is a valid mnemonic
	*/
	public static func validate(mnemonic: String) -> Bool {
		return Mnemonic.validate(mnemonic: mnemonic)
	}
}
