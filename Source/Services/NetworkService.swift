//
//  NetworkService.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 18/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

/// Class responsible for sending all the networking requests, checking for http errors, RPC errors, Decoding the responses and optionally logging progress
public class NetworkService {
	
	// MARK: - Types
	
	/// Errors that can be returned by the `NetworkService`
	public enum NetworkError: Error {
		case parse(error: String)
		case invalidURL
		case unknown
		case httpError(statusCode: Int, response: String?)
	}
	
	
	
	// MARK: - Public Properties
	
	/// The `URLSession` used to preform all the networking operations
	public let urlSession: URLSession
	
	/// The `URLSession` used to preform all the networking operations
	public let loggingConfig: LoggingConfig
	
	
	
	// MARK: - Init
	
	/**
	Init an `NetworkService` with a `URLSession`
	- parameter urlSession: A `URLSession` object
	*/
	init(urlSession: URLSession, loggingConfig: LoggingConfig) {
		self.urlSession = urlSession
		self.loggingConfig = loggingConfig
	}
	
	
	
	// MARK: - Functions
	
	/**
	A generic send function that takes an RPC, with a generic type conforming to `Decodable`, executes the request and returns the result.
	- parameter rpc: A instance of `RPC`.
	- parameter withBaseURL: The base URL needed. This will typically come from `TezosNodeConfig`.
	- parameter completion: A completion callback that will be executed on the main thread.
	- returns: Void
	*/
	func send<T: Decodable>(rpc: RPC<T>, withBaseURL baseURL: URL, completion: @escaping ((Result<T, Error>) -> Void)) {
		let fullURL = baseURL.appendingPathComponent(rpc.endpoint)
		
		var request = URLRequest(url: fullURL)
		request.addValue("application/json", forHTTPHeaderField: "Accept")
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		request.httpMethod = rpc.isPost ? "POST" : "GET"
		
		if let payload = rpc.payload {
			request.httpBody = payload
			request.cachePolicy = .reloadIgnoringCacheData
		}
		
		
		urlSession.dataTask(with: request) { [weak self] (data, response, error) in
			if let err = error {
				self?.logRequestFailed(isPost: rpc.isPost, fullURL: fullURL, payload: rpc.payload, error: err, statusCode: nil, responseData: data)
				DispatchQueue.main.async { completion(Result.failure(err)) }
				
			} else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
				self?.logRequestFailed(isPost: rpc.isPost, fullURL: fullURL, payload: rpc.payload, error: error, statusCode: httpResponse.statusCode, responseData: data)
				DispatchQueue.main.async { completion(Result.failure(NetworkError.httpError(statusCode: httpResponse.statusCode, response: String(data: data ?? Data(), encoding: .utf8)))) }
				
			} else if let d = data {
				self?.logRequestSucceded(isPost: rpc.isPost, fullURL: fullURL, payload: rpc.payload, responseData: data)
				
				do {
					let parsedResponse = try JSONDecoder().decode(T.self, from: d)
					DispatchQueue.main.async { completion(Result.success(parsedResponse)) }
					
				} catch (let error) {
					DispatchQueue.main.async { completion(Result.failure(NetworkError.parse(error: "\(error)"))) }
				}
				
			} else {
				DispatchQueue.main.async { completion(Result.failure(NetworkError.unknown)) }
			}
		}.resume()
		logRequestStart(fullURL: fullURL)
	}
	
	
	
	// MARK: - Logging
	
	/// Logging details of request failures using `os_log` global logging
	private func logRequestFailed(isPost: Bool, fullURL: URL, payload: Data?, error: Error?, statusCode: Int?, responseData: Data?) {
		if !loggingConfig.logNetworkFailures { return }
		
		let errorString = "\(String(describing: error))"
		let payloadString = String(data: payload ?? Data(), encoding: .utf8) ?? ""
		let dataString = String(data: responseData ?? Data(), encoding: .utf8) ?? ""
		
		if isPost {
			os_log(.error, log: .network, "Request Failed to: %@ \nRequest Body: %@ \nError: %@ \nStatusCode: %@ \nResponse: %@ \n_", fullURL.absoluteString, payloadString, errorString, "\(String(describing: statusCode))", dataString)
		} else {
			os_log(.error, log: .network, "Request Failed to: %@ \nError: %@ \nResponse: %@ \n_", fullURL.absoluteString, errorString, dataString)
		}
	}
	
	/// Logging details of successful requests using `os_log` global logging
	private func logRequestSucceded(isPost: Bool, fullURL: URL, payload: Data?, responseData: Data?) {
		if !loggingConfig.logNetworkSuccesses { return }
		
		let payloadString = String(data: payload ?? Data(), encoding: .utf8) ?? ""
		let dataString = String(data: responseData ?? Data(), encoding: .utf8) ?? ""
		
		if isPost {
			os_log(.debug, log: .network, "Request Succeeded to: %@ \nRequest Body: %@ \nResponse: %@ \n_", fullURL.absoluteString, payloadString, dataString)
		} else {
			os_log(.debug, log: .network, "Request Succeeded to: %@ \nResponse: %@ \n_", fullURL.absoluteString, dataString)
		}
	}
	
	/// Logging details when a request starts using `os_log` global logging
	private func logRequestStart(fullURL: URL) {
		if !loggingConfig.logNetworkFailures && !loggingConfig.logNetworkSuccesses  { return }
		
		os_log(.debug, log: .network, "Sending request to: %@", fullURL.absoluteString)
	}
}
