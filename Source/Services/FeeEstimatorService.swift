//
//  FeeEstimatorService.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 18/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

/// An object allowing developers to automatically estimate the necessary fee per Operation to ensure it will be accpeted by a Baker.
/// This avoids the need to ask users to enter a fee, which is not a very user friendly approach as most users wouldn't know what is required.
public class FeeEstimatorService {
	
	/// The real signature is not needed for estimation, use the default "Zero Signature" instead
	public static let defaultSignature = [UInt8](repeating: 0, count: 64)
	
	
	
	// MARK: - Types
	
	/// Constants needed to compute a fee
	public struct FeeConstants {
		static let nanoTezPerMutez: Int = 1000
		static let minimalFee: NanoTez = 100_000
		static let feePerGasUnit: NanoTez = 100
		static let feePerStorageByte: NanoTez = 1000
		static let baseFee = XTZAmount(fromNormalisedAmount: 0.0001)
		static let feePerPaidStorageSizeDiffByte = XTZAmount(fromNormalisedAmount: 0.001)
		static let allocationFee = XTZAmount(fromNormalisedAmount: 0.257)
	}
	
	/// Estimations can be slightly off, use safety margins to ensure they are high enough
	public struct SafetyMargin {
		static let gas = 500
		static let storage = 257
	}
	
	/// Various possible errors that can occur during an Estimation
	public enum FeeEstimatorServiceError: Error {
		case tezosNodeClientNotPresent
		case unableToSetupRunOperation
		case invalidNumberOfFeesReturned
		case estimationRemoteError(errors: [OperationResponseInternalResultError]?)
	}
	
	
	
	// MARK: - Public Properties
	
	/// The `TezosNodeClient` used to perform the forging.
	public weak var tezosNodeClient: TezosNodeClient?
	
	/// The `NetworkService` that will handle the remote communication.
	public let networkService: NetworkService
	
	
	/**
	Create a FeeEstimatorService that will allow developers to automatically create fees on the users behalf
	- parameter tezosNodeClient: The `TezosNodeClient` used to perform the forging.
	- parameter tezosNodeClient: The `NetworkService` that will handle the remote communication.
	*/
	public init(tezosNodeClient: TezosNodeClient?, networkService: NetworkService) {
		self.tezosNodeClient = tezosNodeClient
		self.networkService = networkService
	}
	
	
	/**
	Pass in an array of `Operation` subclasses (use `OperationFacotry` to create) to have the library estimate the cost of sending the transaction.
	- parameter operations: An array of `Operation` subclasses to be estimated.
	- parameter operationMetadata: An `OperationMetadata` object containing necessary info about the current blockchain state.
	- parameter withWallet: The `Wallet` object used for signing the transaction.
	- parameter completion: A callback containing the same operations passed in, modified to include fees.
	*/
	func estimate(operations: [Operation], operationMetadata: OperationMetadata, withWallet wallet: Wallet, completion: @escaping ((Result<[Operation], Error>) -> Void)) {
		
		// Before estimating, set the maximum gas to ensure the operation suceeds (excluding any errors such as invalid address, insufficnet funds etc)
		operations.forEach { $0.operationFees = OperationFees.maxGas() }
		var operationPayload = OperationFactory.operationPayload(fromMetadata: operationMetadata, andOperations: operations, withWallet: wallet)
			
		
		// Forge the payload, for storage cost calculation later
		// May need to forge each operation separately when we integrate Dexter contracts with multiple operations per request
		guard let client = tezosNodeClient else {
			completion(Result.failure(FeeEstimatorServiceError.tezosNodeClientNotPresent))
			return
		}
		
		client.forge(operationMetadata: operationMetadata, operationPayload: operationPayload, wallet: wallet) { [weak self] (forgeResult) in
				
			switch forgeResult {
				case .success(let hexString):
					
					// Add signature to operation payload and perform the /run_operation request
					operationPayload.addSignature(FeeEstimatorService.defaultSignature, signingCurve: wallet.secretKey.signingCurve)
					let runOperationPayload = RunOperationPayload(chainID: operationMetadata.chainID, operation: operationPayload)
				
					self?.estimate(runOperationPayload: runOperationPayload, operations: operations, forgedHex: hexString, completion: completion)
				
				case .failure(let error):
					completion(Result.failure(error))
					return
			}
		}
	}
	
	/// Breaking out part of the estimation process to keep code cleaner
	private func estimate(runOperationPayload: RunOperationPayload, operations: [Operation], forgedHex: String, completion: @escaping ((Result<[Operation], Error>) -> Void)) {
		guard let rpc = RPC.runOperation(runOperationPayload: runOperationPayload) else {
			os_log(.error, log: .camlKit, "Unable to create runOperation RPC, cancelling event")
			completion(Result.failure(FeeEstimatorServiceError.unableToSetupRunOperation))
			return
		}
		
		guard let client = tezosNodeClient else {
			completion(Result.failure(FeeEstimatorServiceError.tezosNodeClientNotPresent))
			return
		}
		
		self.networkService.send(rpc: rpc, withBaseURL: client.config.primaryNodeURL) { [weak self] (result) in
			switch result {
				case .success(let operationResponse):
					
					// Search for errors inside each `OperationResponse`, if theres an error present return them all
					let errors = operationResponse.errors()
					if errors.count > 0 {
						completion(Result.failure(FeeEstimatorServiceError.estimationRemoteError(errors: errors)))
						return
					}
					
					// Extract gas, storage, burn, allocation etc, fees from the response body
					guard let fees = self?.extractFees(fromOperationResponse: operationResponse, forgedHash: forgedHex) else {
						completion(Result.failure(FeeEstimatorServiceError.invalidNumberOfFeesReturned))
						return
					}
					
					// Make sure we have created a `OperationFees` for each operation
					if fees.count != operations.count {
						completion(Result.failure(FeeEstimatorServiceError.invalidNumberOfFeesReturned))
					}
					
					// Add the fee to the corresponding `Operation`
					for (index, op) in operations.enumerated() {
						op.operationFees = fees[index]
					}
					
					completion(Result.success(operations))
					
				case .failure(let error):
					os_log(.error, log: .camlKit, "Unable to remote forge: %@", "\(error)")
					completion(Result.failure(error))
					return
			}
		}
	}
	
	/**
	Create an array of `OperationFees` from an `OperationResponse`.
	- parameter fromOperationResponse: The `OperationResponse` resulting from an RPC call to `.../run_operation`.
	- parameter forgedHash: The forged hash string resulting from a call to `TezosNodeClient.forge(...)`
	- returns: An array of `OperationFees`
	*/
	public func extractFees(fromOperationResponse operationResponse: OperationResponse, forgedHash: String) -> [OperationFees] {
		var opFees: [OperationFees] = []
		
		for content in operationResponse.contents {
			var totalGas = 0
			var totalStorage = 0
			var totalBurnFee = 0
			var totalAllocationFee = XTZAmount.zero()
			
			let results = extractAndParseAttributes(fromResult: content.metadata.operationResult)
			totalGas = results.consumedGas
			totalStorage = results.sotrageSize
			totalBurnFee = results.burnFee
			totalAllocationFee = results.allocationFee
			
			if let innerOperationResults = content.metadata.internalOperationResults {
				for innerResult in innerOperationResults {
					let results = extractAndParseAttributes(fromResult: innerResult.result)
					totalGas += results.consumedGas
					totalStorage += results.sotrageSize
					totalBurnFee += results.burnFee
					totalAllocationFee += results.allocationFee
				}
			}
			
			totalGas += SafetyMargin.gas
			totalStorage += SafetyMargin.storage
			
			let gasFee = feeForGas(totalGas + SafetyMargin.gas)
			let storageFee = feeForStorage(forgedHash)
			let burnFee = feeForBurn(totalBurnFee)
			let networkFees = [[OperationFees.NetworkFeeType.burnFee: burnFee, OperationFees.NetworkFeeType.allocationFee: totalAllocationFee]]
			
			opFees.append(OperationFees(transactionFee: FeeConstants.baseFee + gasFee + storageFee, networkFees: networkFees, gasLimit: totalGas, storageLimit: totalStorage))
		}
		
		return opFees
	}
	
	/// Private helper to process `OperationResponseResult` block. Complicated operations will contain many of these.
	private func extractAndParseAttributes(fromResult result: OperationResponseResult) -> (consumedGas: Int, sotrageSize: Int, burnFee: Int, allocationFee: XTZAmount) {
		let consumedGas = Int(result.consumedGas ?? "0") ?? 0
		let storageSize = Int(result.storageSize ?? "0") ?? 0
		var burnFee = Int(result.paidStorageSizeDiff ?? "0") ?? 0
		var allocationFee = XTZAmount.zero()
		
		if let paidStorageSizeDiff = result.paidStorageSizeDiff, let paidInt = Int(paidStorageSizeDiff) {
			burnFee = paidInt
		}
		
		if let allocated = result.allocatedDestinationContract, allocated {
			allocationFee = FeeConstants.allocationFee
		}
		
		return (consumedGas: consumedGas, sotrageSize: storageSize, burnFee: burnFee, allocationFee: allocationFee)
	}
	
	/// Calculate the fee to add for the given amount of gas
	private func feeForGas(_ gas: Int) -> XTZAmount {
		let nanoTez = gas * FeeConstants.feePerGasUnit
		return nanoTeztoXTZ(nanoTez)
	}
	
	/// Calculate the fee to add based on the size of the forged string
	private func feeForStorage(_ forgedHexString: String) -> XTZAmount {
		let forgedHexWithSignature = (forgedHexString + "\(FeeEstimatorService.defaultSignature)")
		let nanoTez = (forgedHexWithSignature.count/2) * FeeConstants.feePerStorageByte // Multiply bytes (2 characters per byte) by the fee perSotrageByteConstant
		return nanoTeztoXTZ(nanoTez)
	}
	
	/// Calculate the fee to add based on how many bytes of storage where needed
	private func feeForBurn(_ burn: Int) -> XTZAmount {
		return FeeConstants.feePerPaidStorageSizeDiffByte * burn
	}
	
	/// Most calcualtions are documented in NanoTez, which is not accpeted by the network RPC calls. Needs to be converted to Mutez / XTZ
	private func nanoTeztoXTZ(_ nanoTez: NanoTez) -> XTZAmount {
		let mutez = nanoTez % FeeConstants.nanoTezPerMutez == 0 ?
			nanoTez / FeeConstants.nanoTezPerMutez :
			(nanoTez / FeeConstants.nanoTezPerMutez) + 1
		
		return XTZAmount(fromRpcAmount: Decimal(mutez)) ?? XTZAmount.zero()
	}
}







/*
let json = "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"fee\":\"1477\",\"counter\":\"627192\",\"gas_limit\":\"10500\",\"storage_limit\":\"257\",\"amount\":\"500000\",\"destination\":\"tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-1477\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":327,\"change\":\"1477\"}],\"operation_result\":{\"status\":\"applied\",\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-500000\"},{\"kind\":\"contract\",\"contract\":\"tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF\",\"change\":\"500000\"}],\"consumed_gas\":\"10207\"}}}]}"

// [camlKit.OperationFees(transactionFee: 1467, networkFees: [[camlKit.OperationFees.NetworkFeeType.allocationFee: 0, camlKit.OperationFees.NetworkFeeType.burnFee: 0]], gasLimit: 10207, storageLimit: 0)]
*/






/*
let json = "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"fee\":\"0\",\"counter\":\"627191\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"1000000\",\"destination\":\"KT1Ft3X9Lbdcc5GMCgvZcjFzGkeJY7sLKYqW\",\"parameters\":{\"entrypoint\":\"xtzToToken\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\"},{\"prim\":\"Pair\",\"args\":[{\"int\":\"6951\"},{\"string\":\"2020-08-25T12:28:12Z\"}]}]}},\"metadata\":{\"balance_updates\":[],\"operation_result\":{\"status\":\"applied\",\"storage\":{\"prim\":\"Pair\",\"args\":[{\"int\":\"10602\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"int\":\"1000881018\"}]}]},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"0000471c8882bcf12586e640b7efa46c6ea1e0f4da9e\"},{\"bytes\":\"01811c1eab51c1fd74195346ab33ae00d63eac5c2700\"}]},{\"prim\":\"Pair\",\"args\":[{\"int\":\"8372892\"},{\"int\":\"1197050000\"}]}]}]}]},\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-1000000\"},{\"kind\":\"contract\",\"contract\":\"KT1Ft3X9Lbdcc5GMCgvZcjFzGkeJY7sLKYqW\",\"change\":\"1000000\"}],\"consumed_gas\":\"276901\",\"storage_size\":\"9834\"},\"internal_operation_results\":[{\"kind\":\"transaction\",\"source\":\"KT1Ft3X9Lbdcc5GMCgvZcjFzGkeJY7sLKYqW\",\"nonce\":0,\"amount\":\"0\",\"destination\":\"KT1LMSRJukBcS5Z6XYaksA6FaFWX7f3B9n1d\",\"parameters\":{\"entrypoint\":\"transfer\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"01500d452eb9f13f627db4b7444974577e7d0d8dda00\"},{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"0000ca1c5dd7f6665501b57c692f0726a1db46fd1d18\"},{\"int\":\"6979\"}]}]}},\"result\":{\"status\":\"applied\",\"storage\":{\"prim\":\"Pair\",\"args\":[{\"int\":\"10601\"},{\"int\":\"10000000\"}]},\"big_map_diff\":[{\"action\":\"update\",\"big_map\":\"10601\",\"key_hash\":\"exprtkFpPpLkPNbxReKT1osWCfseKDUyyxb7URVBaX2Yw1N5Sq86hv\",\"key\":{\"bytes\":\"0000ca1c5dd7f6665501b57c692f0726a1db46fd1d18\"},\"value\":{\"prim\":\"Pair\",\"args\":[[],{\"int\":\"6979\"}]}},{\"action\":\"update\",\"big_map\":\"10601\",\"key_hash\":\"expruEgEj53rAwspqjPjBN9f8eU5MDy8WuGUxUZs81UQroNHFVXPUi\",\"key\":{\"bytes\":\"01500d452eb9f13f627db4b7444974577e7d0d8dda00\"},\"value\":{\"prim\":\"Pair\",\"args\":[[],{\"int\":\"8372892\"}]}}],\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-75000\"}],\"consumed_gas\":\"94445\",\"storage_size\":\"3638\",\"paid_storage_size_diff\":\"75\"}}]}}]}"
*/

/*
let forgedHash = "16f7879e79ca9aff12eeaf33083d0f3333709b5c2733ea4cde139c8e2ab115f96c00ca1c5dd7f6665501b57c692f0726a1db46fd1d1800f6a3260000c0843d01500d452eb9f13f627db4b7444974577e7d0d8dda00ffff0a78747a546f546f6b656e0000004907070100000024747a3165346841703778706a656b6d586e596534363737454c4741335578523739454662070700a76c0100000014323032302d30382d32355431323a32383a31325a"
*/

// [camlKit.OperationFees(transactionFee: 37635, networkFees: [[camlKit.OperationFees.NetworkFeeType.allocationFee: 0, camlKit.OperationFees.NetworkFeeType.burnFee: 75000]], gasLimit: 371346, storageLimit: 13472)]







/*
"{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"fee\":\"1477\",\"counter\":\"627192\",\"gas_limit\":\"10500\",\"storage_limit\":\"257\",\"amount\":\"250000\",\"destination\":\"tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-1477\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":327,\"change\":\"1477\"}],\"operation_result\":{\"status\":\"applied\",\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-250000\"},{\"kind\":\"contract\",\"contract\":\"tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF\",\"change\":\"250000\"}],\"consumed_gas\":\"10207\"}}},{\"kind\":\"transaction\",\"source\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"fee\":\"1477\",\"counter\":\"627193\",\"gas_limit\":\"10500\",\"storage_limit\":\"257\",\"amount\":\"100000\",\"destination\":\"tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-1477\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":327,\"change\":\"1477\"}],\"operation_result\":{\"status\":\"applied\",\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-100000\"},{\"kind\":\"contract\",\"contract\":\"tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF\",\"change\":\"100000\"}],\"consumed_gas\":\"10207\"}}}]}"
*/

// [camlKit.OperationFees(transactionFee: 1467, networkFees: [[camlKit.OperationFees.NetworkFeeType.allocationFee: 0, camlKit.OperationFees.NetworkFeeType.burnFee: 0]], gasLimit: 10207, storageLimit: 0), camlKit.OperationFees(transactionFee: 1467, networkFees: [[camlKit.OperationFees.NetworkFeeType.allocationFee: 0, camlKit.OperationFees.NetworkFeeType.burnFee: 0]], gasLimit: 10207, storageLimit: 0)]






/*
do {
	let response = try JSONDecoder().decode(OperationResponse.self, from: json.data(using: .utf8) ?? Data())
	let fees = extractFees(fromOperationResponse: response)
	print("Fees: \(fees)")
	
} catch (let error) {
	print("Error: \(error)")
}
*/
